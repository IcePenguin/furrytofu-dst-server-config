--There are two functions that will install mods, ServerModSetup and ServerModCollectionSetup. Put the calls to the functions in this file and they will be executed on boot.

--ServerModSetup takes a string of a specific mod's Workshop id. It will download and install the mod to your mod directory on boot.
	--The Workshop id can be found at the end of the url to the mod's Workshop page.
	--Example: http://steamcommunity.com/sharedfiles/filedetails/?id=350811795
	--ServerModSetup("350811795")

--ServerModCollectionSetup takes a string of a specific mod's Workshop id. It will download all the mods in the collection and install them to the mod directory on boot.
	--The Workshop id can be found at the end of the url to the collection's Workshop page.
	--Example: http://steamcommunity.com/sharedfiles/filedetails/?id=379114180
	--ServerModCollectionSetup("379114180")

ServerModSetup("1207269058") --Simple Health Bar DST
ServerModSetup("1242093526") --Eternal Glowcaps & Mushlights
ServerModSetup("1416161108") --Camp Security
ServerModSetup("1546204443") --Hamlet Turfs
ServerModSetup("2189004162") --Insight (Show Me+)
ServerModSetup("356420397")  --No More Respawn Penalty
ServerModSetup("375850593")  --Extra Equip Slots
ServerModSetup("378160973")  --Global Positions
ServerModSetup("569043634")  --Campfire Respawn
ServerModSetup("1952248433") --Wolly Logger
ServerModSetup("609675532")  --Limit Prefab
